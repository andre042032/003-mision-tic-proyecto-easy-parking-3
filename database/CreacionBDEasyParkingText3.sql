CREATE DATABASE `prueba_tres_dos`;
CREATE TABLE parqueadero (
  id INTEGER   NOT NULL ,
  nit INTEGER    ,
  nombre VARCHAR(255)    ,
  regimen VARCHAR(255)      ,
PRIMARY KEY(id));


CREATE TABLE tipo_vehiculo (
  id_tipo INTEGER   NOT NULL ,
  tipo_nombre TEXT    ,
  plazas_totales INTEGER    ,
  valor_minuto DOUBLE    ,
  valor_mensualidad DOUBLE    ,
  valor_plena DOUBLE    ,
  tipo_servicio BOOL    ,
  porcentaje_iva DOUBLE      ,
PRIMARY KEY(id_tipo));




CREATE TABLE usuarios (
  tipo INTEGER   NOT NULL ,
  documento_identidad VARCHAR(255)   NOT NULL   ,
  hash_password VARCHAR(255)   NOT NULL ,
  nombre VARCHAR(255)   NOT NULL ,
  correo VARCHAR(50)    ,
PRIMARY KEY(tipo));




CREATE TABLE vehiculo (
  id_vehiculo INTEGER   NOT NULL ,
  tipo_vehiculo_id_tipo INTEGER   NOT NULL ,
  placa VARCHAR(7)    ,
  documento INTEGER      ,
PRIMARY KEY(id_vehiculo)  ,
  FOREIGN KEY(tipo_vehiculo_id_tipo)
    REFERENCES tipo_vehiculo(id_tipo));


CREATE INDEX VEHICULO_FKIndex1 ON vehiculo (tipo_vehiculo_id_tipo);


CREATE INDEX IFK_Rel_08 ON vehiculo (tipo_vehiculo_id_tipo);


CREATE TABLE plaza (
  id_plaza INTEGER   NOT NULL ,
  tipo_vehiculo_id_tipo INTEGER   NOT NULL ,
  estado BOOL    ,
  nombre VARCHAR(10)      ,
PRIMARY KEY(id_plaza)  ,
  FOREIGN KEY(tipo_vehiculo_id_tipo)
    REFERENCES tipo_vehiculo(id_tipo));


CREATE INDEX la_plaza_FKIndex1 ON plaza (tipo_vehiculo_id_tipo);


CREATE INDEX IFK_Rel_08 ON plaza (tipo_vehiculo_id_tipo);


CREATE TABLE movimiento_paqueadero (
  id_plaza INTEGER   NOT NULL ,
  plaza_id_plaza INTEGER   NOT NULL ,
  vehiculo_id_vehiculo INTEGER   NOT NULL ,
  hora_entrada DATETIME   ,
  hora_salida DATETIME    ,
  borrado BOOL  DEFAULT false  ,
  ticket_parqueo VARCHAR(255)      ,
PRIMARY KEY(id_plaza)    ,
  FOREIGN KEY(vehiculo_id_vehiculo)
    REFERENCES vehiculo(id_vehiculo),
  FOREIGN KEY(plaza_id_plaza)
    REFERENCES plaza(id_plaza));


CREATE INDEX PLAZA_FKIndex1 ON movimiento_paqueadero (vehiculo_id_vehiculo);
CREATE INDEX Movimiento_Paqueadero_FKIndex2 ON movimiento_paqueadero (plaza_id_plaza);


CREATE INDEX IFK_Rel_02 ON movimiento_paqueadero (vehiculo_id_vehiculo);
CREATE INDEX IFK_Rel_07 ON movimiento_paqueadero (plaza_id_plaza);


CREATE TABLE factura (
  id INTEGER   NOT NULL ,
  vehiculo_id_vehiculo INTEGER   NOT NULL ,
  movimiento_paqueadero_id_plaza INTEGER   NOT NULL ,
  parqueadero_id INTEGER   NOT NULL ,
  fecha DATETIME    ,
  concepto VARCHAR(255)    ,
  tiempo_parqueo INTEGER    ,
  subtotal DOUBLE    ,
  iva DOUBLE    ,
  total DOUBLE    ,
  resolucion VARCHAR(255)    ,
  comentario TEXT    ,
  anulado BOOL  DEFAULT false    ,
PRIMARY KEY(id)      ,
  FOREIGN KEY(parqueadero_id)
    REFERENCES parqueadero(id),
  FOREIGN KEY(movimiento_paqueadero_id_plaza)
    REFERENCES movimiento_paqueadero(id_plaza),
  FOREIGN KEY(vehiculo_id_vehiculo)
    REFERENCES vehiculo(id_vehiculo));


CREATE INDEX Factura_FKIndex1 ON factura (parqueadero_id);
CREATE INDEX Factura_FKIndex2 ON factura (movimiento_paqueadero_id_plaza);
CREATE INDEX factura_FKIndex3 ON factura (vehiculo_id_vehiculo);


CREATE INDEX IFK_Rel_05 ON factura (parqueadero_id);
CREATE INDEX IFK_Rel_08 ON factura (movimiento_paqueadero_id_plaza);
CREATE INDEX IFK_Rel_07 ON factura (vehiculo_id_vehiculo);


