-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: maria-misiontic.c13c7jwssaod.us-east-1.rds.amazonaws.com    Database: easyparking
-- ------------------------------------------------------
-- Server version	5.5.5-10.6.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movimiento_paqueadero_id_plaza` int(11) NOT NULL,
  `parqueadero_id` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `concepto` varchar(255) DEFAULT NULL,
  `tiempo_parqueo` int(11) DEFAULT NULL,
  `subtotal` double DEFAULT NULL,
  `iva` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `resolucion` varchar(255) DEFAULT NULL,
  `comentario` text DEFAULT NULL,
  `anulado` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `Factura_FKIndex1` (`parqueadero_id`),
  KEY `Factura_FKIndex2` (`movimiento_paqueadero_id_plaza`),
  KEY `IFK_Rel_05` (`parqueadero_id`),
  KEY `IFK_Rel_08` (`movimiento_paqueadero_id_plaza`),
  CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`parqueadero_id`) REFERENCES `parqueadero` (`id`),
  CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`movimiento_paqueadero_id_plaza`) REFERENCES `movimiento_paqueadero` (`id_plaza`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES (1,1,1,'2022-09-01 12:00:00','Parqueadero-Normal',60,18000,3420,21420,'Resolución DIAN Nro 192478 fecha 2022/05/12 habilitado desde 1 al 12903210','Sotware por Grupos Aliados SAS',0),(2,2,1,'2022-09-02 15:00:00','Parqueadero-Normal',10,10000,1900,11900,'Resolución DIAN Nro 19247384956278 fecha 2022/05/12 habilitado desde 1 al 12903211','Sotware por Grupos Aliados SAS',0),(3,3,1,'2022-08-03 14:00:00','Parqueadero-Normal',90,2000,380,2380,'Resolución DIAN Nro 19247384956278 fecha 2022/05/12 habilitado desde 1 al 12903212','Sotware por Grupos Aliados SAS',0),(4,4,1,'2022-09-05 13:00:00','Parqueadero-Mensual',60,10000,1900,11900,'Resolución DIAN Nro 19247384956278 fecha 2022/05/12 habilitado desde 1 al 12903213','Sotware por Grupos Aliados SAS',0),(5,5,1,'2022-09-06 11:00:00','Parqueadero-Normal',30,2000,380,2380,'Resolución DIAN Nro 19247384956278 fecha 2022/05/12 habilitado desde 1 al 12903214','Sotware por Grupos Aliados SAS',0);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento_paqueadero`
--

DROP TABLE IF EXISTS `movimiento_paqueadero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimiento_paqueadero` (
  `id_plaza` int(11) NOT NULL AUTO_INCREMENT,
  `plaza_id_plaza` int(11) NOT NULL,
  `vehiculo_id_vehiculo` int(11) NOT NULL,
  `hora_entrada` datetime DEFAULT NULL,
  `hora_salida` datetime DEFAULT NULL,
  `borrado` tinyint(1) DEFAULT 0,
  `ticket_parqueo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_plaza`),
  KEY `PLAZA_FKIndex1` (`vehiculo_id_vehiculo`),
  KEY `Movimiento_Paqueadero_FKIndex2` (`plaza_id_plaza`),
  KEY `IFK_Rel_02` (`vehiculo_id_vehiculo`),
  KEY `IFK_Rel_07` (`plaza_id_plaza`),
  CONSTRAINT `movimiento_paqueadero_ibfk_1` FOREIGN KEY (`vehiculo_id_vehiculo`) REFERENCES `vehiculo` (`id_vehiculo`),
  CONSTRAINT `movimiento_paqueadero_ibfk_2` FOREIGN KEY (`plaza_id_plaza`) REFERENCES `plaza` (`id_plaza`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento_paqueadero`
--

LOCK TABLES `movimiento_paqueadero` WRITE;
/*!40000 ALTER TABLE `movimiento_paqueadero` DISABLE KEYS */;
INSERT INTO `movimiento_paqueadero` VALUES (1,1,1,'2022-09-06 12:00:00','2022-09-06 13:00:00',0,'1'),(2,2,2,'2022-09-06 11:30:00','2022-09-06 11:40:00',0,'2'),(3,3,3,'2022-09-06 13:15:00','2022-09-06 12:30:00',0,'3'),(4,4,4,'2022-09-06 15:00:00','2022-09-06 16:00:00',0,'4'),(5,5,5,'2022-09-06 14:15:00','2022-09-06 14:45:00',0,'5');
/*!40000 ALTER TABLE `movimiento_paqueadero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parqueadero`
--

DROP TABLE IF EXISTS `parqueadero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parqueadero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nit` int(11) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `regimen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parqueadero`
--

LOCK TABLES `parqueadero` WRITE;
/*!40000 ALTER TABLE `parqueadero` DISABLE KEYS */;
INSERT INTO `parqueadero` VALUES (1,900849503,'Easy Parking','Responsable'),(2,123456444,'ParKing','Responsable'),(3,88662244,'UltiParking','Responsable');
/*!40000 ALTER TABLE `parqueadero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plaza`
--

DROP TABLE IF EXISTS `plaza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plaza` (
  `id_plaza` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_vehiculo_id_tipo` int(11) NOT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `nombre` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_plaza`),
  KEY `la_plaza_FKIndex1` (`tipo_vehiculo_id_tipo`),
  KEY `IFK_Rel_08` (`tipo_vehiculo_id_tipo`),
  CONSTRAINT `plaza_ibfk_1` FOREIGN KEY (`tipo_vehiculo_id_tipo`) REFERENCES `tipo_vehiculo` (`id_tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plaza`
--

LOCK TABLES `plaza` WRITE;
/*!40000 ALTER TABLE `plaza` DISABLE KEYS */;
INSERT INTO `plaza` VALUES (1,1,0,'A1'),(2,2,0,'B2'),(3,3,0,'C1'),(4,2,0,'B3'),(5,3,0,'C2');
/*!40000 ALTER TABLE `plaza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_vehiculo`
--

DROP TABLE IF EXISTS `tipo_vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_vehiculo` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_nombre` text DEFAULT NULL,
  `plazas_totales` int(11) DEFAULT NULL,
  `valor_minuto` double DEFAULT NULL,
  `valor_mensualidad` double DEFAULT NULL,
  `valor_plena` double DEFAULT NULL,
  `tipo_servicio` tinyint(1) DEFAULT NULL,
  `porcentaje_iva` double DEFAULT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_vehiculo`
--

LOCK TABLES `tipo_vehiculo` WRITE;
/*!40000 ALTER TABLE `tipo_vehiculo` DISABLE KEYS */;
INSERT INTO `tipo_vehiculo` VALUES (1,'carro',120,110,300000,18000,1,0.19),(2,'moto',50,50,150000,10000,0,0.19),(3,'bicicleta',27,10,30000,2000,0,0.19);
/*!40000 ALTER TABLE `tipo_vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `tipo` int(11) NOT NULL AUTO_INCREMENT,
  `documento_identidad` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `hash_password` varchar(255) NOT NULL,
  `correo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'101010101','Alonso Medina','0101alonso','alnso01@gmail.com'),(2,'202020202','Liliana Fonce','0202liliana','liliana02@gmail.com'),(3,'303030303','Luis Cotreras','0303luis','luis03@gmail.com'),(4,'404040404','Amanda Torres','0404amanda','amanda04@gmail.com'),(5,'505050505','Esperanza Rico','0505esperanza','esperanza05@gmail.com');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiculo`
--

DROP TABLE IF EXISTS `vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehiculo` (
  `id_vehiculo` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_vehiculo_id_tipo` int(11) NOT NULL,
  `placa` varchar(7) DEFAULT NULL,
  `documento` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_vehiculo`),
  KEY `VEHICULO_FKIndex1` (`tipo_vehiculo_id_tipo`),
  KEY `IFK_Rel_08` (`tipo_vehiculo_id_tipo`),
  CONSTRAINT `vehiculo_ibfk_1` FOREIGN KEY (`tipo_vehiculo_id_tipo`) REFERENCES `tipo_vehiculo` (`id_tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiculo`
--

LOCK TABLES `vehiculo` WRITE;
/*!40000 ALTER TABLE `vehiculo` DISABLE KEYS */;
INSERT INTO `vehiculo` VALUES (1,1,'ABF123',0),(2,1,'BSD294',0),(3,2,'SDF35G',0),(4,2,'ASD98A',0),(5,3,'0',1234329);
/*!40000 ALTER TABLE `vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'easyparking'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-18 21:12:30
