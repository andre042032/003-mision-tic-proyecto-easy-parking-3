/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.controller;

import com.example.EasyParking.model.Plaza;
import com.example.EasyParking.servicess.PlazaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author USUARIO
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/plaza")
public class PlazaController {
     @Autowired
    private PlazaService plazaService;
    
    @PostMapping(value="/")
    public ResponseEntity<Plaza> agregar(@RequestBody Plaza plaza){
        Plaza obj =plazaService.save(plaza);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
     public ResponseEntity<Plaza> eliminar(@PathVariable Integer id){
         Plaza obj = plazaService.findById(id);
         if(obj!=null){
             plazaService.delete(id);
         }
         else
             return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
         return new ResponseEntity<>(obj, HttpStatus.OK);
     }
     
     @PutMapping(value="/")
     public ResponseEntity<Plaza> editar(@RequestBody Plaza plaza){
         Plaza obj = plazaService.findById(plaza.getId_plaza());
                 
         if(obj!=null) {
             obj.setTipo_vehiculo_id_tipo(plaza.getTipo_vehiculo_id_tipo());
             obj.setTipo_vehiculo_id_tipo(plaza.getTipo_vehiculo_id_tipo());
             obj.setEstado(plaza.getEstado());
             obj.setNombre(plaza.getNombre());
             plazaService.save(obj);
         }   
         else
             return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
         return new ResponseEntity<>(obj, HttpStatus.OK);
     }
     
     @GetMapping("/list")
     public List<Plaza> consultarTodo(){
         return plazaService.findAll();         
     }
     
     @GetMapping("/list/{id}")
     public Plaza consultarPorId(@PathVariable Integer id){
         return plazaService.findById(id);
     }
}
