
package com.example.EasyParking.controller;

import com.example.EasyParking.model.Vehiculo;
import com.example.EasyParking.servicess.VehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/vehiculo")
public class VehiculoController {
    @Autowired
    private VehiculoService vehiculoService;
    
    @PostMapping(value="/")
    public ResponseEntity<Vehiculo> agregar(@RequestBody Vehiculo vehiculo){
        Vehiculo obj = vehiculoService.save(vehiculo);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
     public ResponseEntity<Vehiculo> eliminar(@PathVariable Integer id){
         Vehiculo obj = vehiculoService.findById(id);
         if(obj!=null){
             vehiculoService.delete(id);
         }
         else
             return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
         return new ResponseEntity<>(obj, HttpStatus.OK);
     }
     
     @PutMapping(value="/")
     public ResponseEntity<Vehiculo> editar(@RequestBody Vehiculo vehiculo){
         Vehiculo obj = vehiculoService.findById(vehiculo.getId_vehiculo());
                 
         if(obj!=null) {
             obj.setTipo_vehiculo_id_tipo(vehiculo.getTipo_vehiculo_id_tipo());
             obj.setPlaca(vehiculo.getPlaca());
             obj.setDocumento(vehiculo.getDocumento());
             vehiculoService.save(obj);
         }   
         else
             return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
         return new ResponseEntity<>(obj, HttpStatus.OK);
     }
     
     @GetMapping("/list")
     public List<Vehiculo> consultarTodo(){
         return vehiculoService.findAll();         
     }
     
     @GetMapping("/list/{id}")
     public Vehiculo consultarPorId(@PathVariable Integer id){
         return vehiculoService.findById(id);
     }
     
}
