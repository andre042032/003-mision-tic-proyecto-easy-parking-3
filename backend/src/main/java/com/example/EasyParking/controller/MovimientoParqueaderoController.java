/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.controller;

import com.example.EasyParking.model.MovimientoParqueadero;
import com.example.EasyParking.servicess.MovimientoParqueaderoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author USUARIO
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/movimiento_paqueadero") //<- Base datos está como movimiento_paqueadero
public class MovimientoParqueaderoController {
    @Autowired
    private MovimientoParqueaderoService movimientoParqueaderoService;

    @PostMapping(value = "/")
    public ResponseEntity<MovimientoParqueadero> agregar(@RequestBody MovimientoParqueadero movimientoParqueadero) {
        MovimientoParqueadero obj = movimientoParqueaderoService.save(movimientoParqueadero);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<MovimientoParqueadero> eliminar(@PathVariable Integer id) {
        MovimientoParqueadero obj = movimientoParqueaderoService.findById(id);
        if (obj != null) {
            movimientoParqueaderoService.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<MovimientoParqueadero> editar(@RequestBody MovimientoParqueadero movimientoParqueadero) {
        MovimientoParqueadero obj = movimientoParqueaderoService.findById(movimientoParqueadero.getId_plaza());
        if (obj != null) {
            obj.setPlaza_id_plaza(movimientoParqueadero.getPlaza_id_plaza());
            obj.setVehiculo_id_vehiculo(movimientoParqueadero.getVehiculo_id_vehiculo());
            obj.setHora_entrada(movimientoParqueadero.getHora_entrada());
            obj.setHora_salida(movimientoParqueadero.getHora_salida());
            obj.setBorrado(movimientoParqueadero.getBorrado());
            obj.setTicket_parqueadero(movimientoParqueadero.getTicket_parqueadero());
            
            movimientoParqueaderoService.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<MovimientoParqueadero> consultarTodo() {
        return movimientoParqueaderoService.findAll();
    }
    

    @GetMapping("/list/{id}")
    public MovimientoParqueadero consultaPorId(@PathVariable Integer id) {
        return movimientoParqueaderoService.findById(id);
    }

}
