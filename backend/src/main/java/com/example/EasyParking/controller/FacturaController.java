/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.controller;

import com.example.EasyParking.model.Factura;
import com.example.EasyParking.servicess.FacturaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author USUARIO
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/factura")
public class FacturaController {
     @Autowired
    private FacturaService facturaService;

    @PostMapping(value = "/")
    public ResponseEntity<Factura> agregar(@RequestBody Factura factura) {
        Factura obj = facturaService.save(factura);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Factura> eliminar(@PathVariable Integer id) {
        Factura obj = facturaService.findById(id);
        if (obj != null) {
            facturaService.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Factura> editar(@RequestBody Factura factura) {
        Factura obj = facturaService.findById(factura.getId());
        if (obj != null) {
            obj.setId(factura.getId());
            obj.setMovimiento_parqueadero_id_plaza(factura.getMovimiento_parqueadero_id_plaza());
            obj.setParqueadero_id(factura.getParqueadero_id());            
            obj.setFecha(factura.getFecha());
            obj.setConcepto(factura.getConcepto());
            obj.setTiempo_parqueadero(factura.getTiempo_parqueadero());
            obj.setSubtotal(factura.getSubtotal());
            obj.setIva(factura.getIva());
            obj.setTotal(factura.getTotal());
            obj.setResolucion(factura.getResolucion());
            obj.setComentario(factura.getComentario());
            obj.setAnulado(factura.getAnulado());
            facturaService.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }


    @GetMapping("/list")
    public List<Factura> consultarTodo() {
        return facturaService.findAll();
    }

    @GetMapping("/list/{id}")
    public Factura consultaPorId(@PathVariable Integer id) {
        return facturaService.findById(id);
    }
    
}
