
package com.example.EasyParking.controller;

import com.example.EasyParking.model.TipoVehiculo;
import com.example.EasyParking.servicess.TipoVehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/tipo_vehiculo")
public class TipoVehiculoController {
    @Autowired
    private TipoVehiculoService tipoVehiculoService;

    @PostMapping(value = "/")
    public ResponseEntity<TipoVehiculo> agregar(@RequestBody TipoVehiculo tipoVehiculo) {
        TipoVehiculo obj = tipoVehiculoService.save(tipoVehiculo);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<TipoVehiculo> eliminar(@PathVariable Integer id) {
        TipoVehiculo obj = tipoVehiculoService.findById(id);
        if (obj != null) {
            tipoVehiculoService.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<TipoVehiculo> consultarTodo() {
        return tipoVehiculoService.findAll();
    }

    @GetMapping("/list/{id}")
    public TipoVehiculo consultaPorId(@PathVariable Integer id) {
        return tipoVehiculoService.findById(id);
    }
    
        @PutMapping(value = "/")
    public ResponseEntity<TipoVehiculo> editar(@RequestBody TipoVehiculo tipoVehiculo) {
        TipoVehiculo obj = tipoVehiculoService.findById(tipoVehiculo.getIdTipo());
        if (obj != null) {
            obj.setPlazasTotales(tipoVehiculo.getPlazasTotales());//
            obj.setPorcentajeIva(tipoVehiculo.getPorcentajeIva());//
            obj.setTipoNombre(tipoVehiculo.getTipoNombre());        //    
            obj.setTipoServicio(tipoVehiculo.getTipoServicio());//
            obj.setValorMensualidad(tipoVehiculo.getValorMensualidad());//
            obj.setValorMinuto(tipoVehiculo.getValorMinuto());//
            obj.setValorPlena(tipoVehiculo.getValorPlena());//
            tipoVehiculoService.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

}
