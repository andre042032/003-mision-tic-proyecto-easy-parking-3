
package com.example.EasyParking.controller;

import com.example.EasyParking.model.Parqueadero;
import com.example.EasyParking.servicess.ParqueaderoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author USUARIO
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/parqueadero")
public class ParqueaderoController {
    @Autowired
    private ParqueaderoService parqueaderoService;

    @PostMapping(value = "/")
    public ResponseEntity<Parqueadero> agregar(@RequestBody Parqueadero parqueadero) {
        Parqueadero obj = parqueaderoService.save(parqueadero);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Parqueadero> eliminar(@PathVariable Integer id) {
        Parqueadero obj = parqueaderoService.findById(id);
        if (obj != null) {
            parqueaderoService.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Parqueadero> editar(@RequestBody Parqueadero parqueadero) {
        Parqueadero obj = parqueaderoService.findById(parqueadero.getId());
        if (obj != null) {
            obj.setNit(parqueadero.getNit());
            obj.setNombre(parqueadero.getNombre());
            obj.setRegimen(parqueadero.getRegimen());
            
            parqueaderoService.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Parqueadero> consultarTodo() {
        return parqueaderoService.findAll();
    }
    

    @GetMapping("/list/{id}")
    public Parqueadero consultaPorId(@PathVariable Integer id) {
        return parqueaderoService.findById(id);
    }

}
