/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.controller;

import com.example.EasyParking.model.Usuarios;
import com.example.EasyParking.servicess.UsuariosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author USUARIO
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/usuarios")
public class UsuariosController {
     @Autowired
    private UsuariosService usuariosService;
    
    @PostMapping(value="/")
    public ResponseEntity<Usuarios> agregar(@RequestBody Usuarios usuarios){
        Usuarios obj =usuariosService.save(usuarios);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
     public ResponseEntity<Usuarios> eliminar(@PathVariable Integer id){
         Usuarios obj = usuariosService.findById(id);
         if(obj!=null){
             usuariosService.delete(id);
         }
         else
             return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
         return new ResponseEntity<>(obj, HttpStatus.OK);
     }
     
     @PutMapping(value="/")
     public ResponseEntity<Usuarios> editar(@RequestBody Usuarios usuarios){
         Usuarios obj = usuariosService.findById(usuarios.getTipo());
                 
         if(obj!=null) {
             obj.setDocumento_identidad(usuarios.getDocumento_identidad());
             obj.setNombre(usuarios.getNombre());
             obj.setHash_password(usuarios.getHash_password());
             obj.setCorreo(usuarios.getCorreo());
             usuariosService.save(obj);
         }   
         else
             return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
         return new ResponseEntity<>(obj, HttpStatus.OK);
     }
     
     @GetMapping("/list")
     public List<Usuarios> consultarTodo(){
         return usuariosService.findAll();         
     }
     
     @GetMapping("/list/{id}")
     public Usuarios consultarPorId(@PathVariable Integer id){
         return usuariosService.findById(id);
     }
}
