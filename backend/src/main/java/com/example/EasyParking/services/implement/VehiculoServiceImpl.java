
package com.example.EasyParking.services.implement;

import com.example.EasyParking.dao.VehiculoDao;
import com.example.EasyParking.model.Vehiculo;
import com.example.EasyParking.servicess.VehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author USUARIO
 */
@Service
public class VehiculoServiceImpl implements VehiculoService{
    @Autowired
    
    private VehiculoDao vehiculoDao;
    
    @Override
    @Transactional(readOnly=false)
    public Vehiculo save(Vehiculo vehiculo){
        return vehiculoDao.save(vehiculo);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
    vehiculoDao.deleteById(id);

    }

    @Override
    @Transactional(readOnly=true)
    public Vehiculo findById(Integer id){
        return vehiculoDao.findById(id).orElse(null);
    }
    
    @Override 
    @Transactional(readOnly=true)
    public List<Vehiculo> findAll(){
        return (List<Vehiculo>) vehiculoDao.findAll();
    }

}
