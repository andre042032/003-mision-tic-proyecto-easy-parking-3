/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.services.implement;
import com.example.EasyParking.dao.ParqueaderoDao;
//import com.example.EasyParking.dao.TipoVehiculoDao;
import com.example.EasyParking.model.Parqueadero;
//import com.example.EasyParking.model.TipoVehiculo;
import com.example.EasyParking.servicess.ParqueaderoService;
//import com.example.EasyParking.servicess.TipoVehiculoService;
import java.util.List;
//import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class ParqueaderoServiceImpl implements ParqueaderoService{
    @Autowired
    private ParqueaderoDao parqueaderoDao;
    
    @Override
    @Transactional(readOnly=false)
    public Parqueadero save(Parqueadero parqueadero){
    return parqueaderoDao.save(parqueadero);
    }  
    
    
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        parqueaderoDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Parqueadero findById(Integer id) {
        return parqueaderoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Parqueadero> findAll() {
        return (List<Parqueadero>) parqueaderoDao.findAll();
    }

}
