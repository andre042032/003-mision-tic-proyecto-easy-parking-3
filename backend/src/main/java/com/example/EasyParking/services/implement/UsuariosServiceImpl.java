/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.services.implement;

import com.example.EasyParking.dao.UsuariosDao;
import com.example.EasyParking.model.Usuarios;
import com.example.EasyParking.servicess.UsuariosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author USUARIO
 */
@Service
public class UsuariosServiceImpl implements UsuariosService{
    @Autowired
    private UsuariosDao usuariosDao;
    
    @Override
    @Transactional(readOnly=false)
    public Usuarios save(Usuarios   usuarios){
    return usuariosDao.save(usuarios);
    }  
    
    
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        usuariosDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Usuarios findById(Integer id) {
        return usuariosDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Usuarios> findAll() {
        return (List<Usuarios>) usuariosDao.findAll();
    }
    
}
