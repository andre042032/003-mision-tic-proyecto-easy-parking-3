/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.services.implement;
import com.example.EasyParking.dao.TipoVehiculoDao;
import com.example.EasyParking.model.TipoVehiculo;
import com.example.EasyParking.servicess.TipoVehiculoService;
import java.util.List;
//import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author USUARIO
 */

@Service
public class TipoVehiculoServiceImpl implements TipoVehiculoService{
    @Autowired
    private TipoVehiculoDao tipoVehiculoDao;
    
    @Override
    @Transactional(readOnly=false)
    public TipoVehiculo save(TipoVehiculo tipoVehiculo){
    return tipoVehiculoDao.save(tipoVehiculo);
    }   
    
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        tipoVehiculoDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly = true)
    public TipoVehiculo findById(Integer id) {
        return tipoVehiculoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TipoVehiculo> findAll() {
        return (List<TipoVehiculo>) tipoVehiculoDao.findAll();
    }
    
    

}
