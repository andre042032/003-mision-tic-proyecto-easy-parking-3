/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.services.implement;

import com.example.EasyParking.dao.MovimientoParqueaderoDao;
import com.example.EasyParking.model.MovimientoParqueadero;
import com.example.EasyParking.model.Parqueadero;
import com.example.EasyParking.servicess.MovimientoParqueaderoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author USUARIO
 */
@Service
public class MovimientoParqueaderoServiceImpl implements MovimientoParqueaderoService{
    @Autowired
    private MovimientoParqueaderoDao movimientoParqueaderoDao;
    
    @Override
    @Transactional(readOnly=false)
    public MovimientoParqueadero save(MovimientoParqueadero movimientoParqueadero){
    return movimientoParqueaderoDao.save(movimientoParqueadero);
    }  
    
    
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        movimientoParqueaderoDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly = true)
    public MovimientoParqueadero findById(Integer id) {
        return movimientoParqueaderoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MovimientoParqueadero> findAll() {
        return (List<MovimientoParqueadero>) movimientoParqueaderoDao.findAll();
    }

}
