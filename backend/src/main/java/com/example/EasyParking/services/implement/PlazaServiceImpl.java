/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.services.implement;

import com.example.EasyParking.dao.ParqueaderoDao;
import com.example.EasyParking.dao.PlazaDao;
import com.example.EasyParking.model.Parqueadero;
import com.example.EasyParking.model.Plaza;
import com.example.EasyParking.servicess.PlazaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author USUARIO
 */
@Service
public class PlazaServiceImpl implements PlazaService{
    @Autowired
    private PlazaDao plazaDao;
    
    @Override
    @Transactional(readOnly=false)
    public Plaza save(Plaza plaza){
    return plazaDao.save(plaza);
    }  
    
    
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        plazaDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Plaza findById(Integer id) {
        return plazaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Plaza> findAll() {
        return (List<Plaza>) plazaDao.findAll();
    }

}
