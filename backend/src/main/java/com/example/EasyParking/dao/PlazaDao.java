
package com.example.EasyParking.dao;
import com.example.EasyParking.model.Plaza;
import org.springframework.data.repository.CrudRepository;

/**
 * @author USUARIO
 */
public interface PlazaDao extends CrudRepository<Plaza,Integer>{
    
}
