
package com.example.EasyParking.dao;

import com.example.EasyParking.model.TipoVehiculo;
import org.springframework.data.repository.CrudRepository;

public interface TipoVehiculoDao extends CrudRepository<TipoVehiculo,Integer>{    
    
    
}
