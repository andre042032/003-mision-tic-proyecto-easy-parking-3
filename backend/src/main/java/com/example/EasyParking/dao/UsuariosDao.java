
package com.example.EasyParking.dao;

import com.example.EasyParking.model.Usuarios;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author USUARIO
 */
public interface UsuariosDao extends CrudRepository<Usuarios, Integer>{
    
}
