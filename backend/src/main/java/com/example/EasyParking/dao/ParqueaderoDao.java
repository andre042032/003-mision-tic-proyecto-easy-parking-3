
package com.example.EasyParking.dao;

import com.example.EasyParking.model.Parqueadero;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author USUARIO
 */
public interface ParqueaderoDao extends CrudRepository<Parqueadero, Integer>{
    
}
