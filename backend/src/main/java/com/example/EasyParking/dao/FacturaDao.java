/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.dao;

import com.example.EasyParking.model.Factura;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author USUARIO
 */
public interface FacturaDao extends CrudRepository<Factura, Integer>{
    
}
