package com.example.EasyParking.model;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//import org.hibernate.annotations.GenericGenerator;
/**
 *
 * @author Lindsay Andrea Quintero Hernández
 */
@Entity
@Table(name="vehiculo")
public class Vehiculo implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_vehiculo")
    private Integer id_vehiculo;    
    
    @ManyToOne
    @JoinColumn(name="tipo_vehiculo_id_tipo")
    private TipoVehiculo tipo_vehiculo_id_tipo;
    
    @Column(name="placa")
    private String placa;
    
    @Column(name="documento")
    private Integer documento;

    
    public Integer getId_vehiculo() {
        return id_vehiculo;
    }

    public void setId_vehiculo(Integer id_vehiculo) {
        this.id_vehiculo = id_vehiculo;
    }

    public TipoVehiculo getTipo_vehiculo_id_tipo() {
        return tipo_vehiculo_id_tipo;
    }

    public void setTipo_vehiculo_id_tipo(TipoVehiculo tipo_vehiculo_id_tipo) {
        this.tipo_vehiculo_id_tipo = tipo_vehiculo_id_tipo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Integer getDocumento() {
        return documento;
    }

    public void setDocumento(Integer documento) {
        this.documento = documento;
    }
    

}
   
