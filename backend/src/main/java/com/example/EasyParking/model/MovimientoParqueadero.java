
package com.example.EasyParking.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Lindsay Andrea Quintero Hernández
 */

@Entity
@Table (name="movimiento_paqueadero") //<.Base datos quedó como movimiento_paqueadero
public class MovimientoParqueadero implements Serializable{
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_plaza")
    private Integer id_plaza;
    
    @ManyToOne
    @JoinColumn(name="plaza_id_plaza")
    private Plaza plaza_id_plaza;
    
    @ManyToOne
    @JoinColumn(name="vehiculo_id_vehiculo")
    private Vehiculo vehiculo_id_vehiculo;
    
    @Column(name="hora_entrada")
    private String hora_entrada; //Tipo de dato

    @Column(name="hora_salida")
    private String hora_salida;
    
    @Column(name="borrado")
    private Boolean borrado;
    
    @Column(name="ticket_parqueo")
    private String ticket_parqueo;

    
    public Integer getId_plaza() {
        return id_plaza;
    }

    public void setId_plaza(Integer id_plaza) {
        this.id_plaza = id_plaza;
    }

    public Plaza getPlaza_id_plaza() {
        return plaza_id_plaza;
    }

    public void setPlaza_id_plaza(Plaza plaza_id_plaza) {
        this.plaza_id_plaza = plaza_id_plaza;
    }

    public Vehiculo getVehiculo_id_vehiculo() {
        return vehiculo_id_vehiculo;
    }

    public void setVehiculo_id_vehiculo(Vehiculo vehiculo_id_vehiculo) {
        this.vehiculo_id_vehiculo = vehiculo_id_vehiculo;
    }

    public String getHora_entrada() {
        return hora_entrada;
    }

    public void setHora_entrada(String hora_entrada) {
        this.hora_entrada = hora_entrada;
    }

    public String getHora_salida() {
        return hora_salida;
    }

    public void setHora_salida(String hora_salida) {
        this.hora_salida = hora_salida;
    }

    public Boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    public String getTicket_parqueadero() {
        return ticket_parqueo;
    }

    public void setTicket_parqueadero(String ticket_parqueadero) {
        this.ticket_parqueo = ticket_parqueadero;
    }
    
    
}
