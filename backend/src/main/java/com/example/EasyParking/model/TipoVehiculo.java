package com.example.EasyParking.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Lindsay Andrea Quintero Hernández
 */
@Entity
@Table(name="tipo_vehiculo")
public class TipoVehiculo implements Serializable{
    
    @Id  
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_tipo")
    private Integer id_tipo;
    
    @Column(name="tipo_nombre")
    private String tipo_nombre;
    
    @Column(name="plazas_totales")
    private Integer plazas_totales;
    
    @Column(name="valor_minuto")
    private double valor_minuto;
      
    @Column(name="valor_mensualidad")
    private double valor_mensualidad;
       
    @Column(name="valor_plena")
    private double valor_plena;
    
    @Column(name="tipo_servicio") 
    private Boolean tipo_servicio;
    
    @Column(name="porcentaje_iva")
    private double porcentaje_iva;
    

    
//    public TipoVehiculo(Integer id_tipo, Integer plazas_totales, double porcentaje_iva, String tipo_nombre, Boolean tipo_servicio, double valor_mensualidad, double valor_minuto, double valor_plena) {
//        this.id_tipo = id_tipo;
//        this.plazas_totales = plazas_totales;
//        this.porcentaje_iva = porcentaje_iva;
//        this.tipo_nombre = tipo_nombre;
//        this.tipo_servicio = tipo_servicio;
//        this.valor_mensualidad = valor_mensualidad;
//        this.valor_minuto = valor_minuto;
//        this.valor_plena = valor_plena;
//    }

    public Integer getIdTipo() {
        return id_tipo;
    }

    public void setId_tipo(Integer id_tipo) {//
        this.id_tipo = id_tipo;//
    }

    public String getTipoNombre() {//
        return tipo_nombre;//
    }//

    public void setTipoNombre(String tipo_nombre) {//
        this.tipo_nombre = tipo_nombre;//
    }//

    public Integer getPlazasTotales() {//
        return plazas_totales;//
    }//

    public void setPlazasTotales(Integer plazas_totales) {//
        this.plazas_totales = plazas_totales;//
    }//

    public double getValorMinuto() {//
        return valor_minuto;//
    }//

    public void setValorMinuto(double valor_minuto) {//
        this.valor_minuto = valor_minuto;//
    }//

    public double getValorMensualidad() {//
        return valor_mensualidad;//
    }

    public void setValorMensualidad(double valor_mensualidad) {//...
        this.valor_mensualidad = valor_mensualidad;//
    }//

    public double getValorPlena() {//
        return valor_plena;//
    }//

    public void setValorPlena(double valor_plena) {//...
        this.valor_plena = valor_plena;//
    }//

    public Boolean getTipoServicio() {//...
        return tipo_servicio;
    }

    public void setTipoServicio(Boolean tipo_servicio) {//...
        this.tipo_servicio = tipo_servicio;
    }

    public double getPorcentajeIva() {//...
        return porcentaje_iva;
    }

    public void setPorcentajeIva(double porcentaje_iva) {//:)
        this.porcentaje_iva = porcentaje_iva;
    }

    
}
