
package com.example.EasyParking.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "factura")
public class Factura implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name="movimiento_parqueadero_id_plaza")
    private MovimientoParqueadero movimiento_parqueadero_id_plaza;
    
    @ManyToOne
    @JoinColumn(name="parqueadero_id")
    private Parqueadero parqueadero_id; 
    
    @Column(name="fecha")
    private String fecha;
    
    @Column(name="concepto")
    private String concepto;
    
    @Column(name="tiempo_parqueo")
    private Integer tiempo_parqueo;
    
    @Column(name="subtotal")
    private double subtotal;
    
    @Column(name="iva")
    private double iva;
    
    @Column(name="total")
    private double total;
    
    @Column(name="resolucion")
    private String resolucion;
    
    @Column(name="comentario")
    private String comentario;
    
    @Column(name="anulado")
    private Boolean anulado;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    
    
    public MovimientoParqueadero getMovimiento_parqueadero_id_plaza() {
        return movimiento_parqueadero_id_plaza;
    }

    public void setMovimiento_parqueadero_id_plaza(MovimientoParqueadero movimiento_parqueadero_id_plaza) {
        this.movimiento_parqueadero_id_plaza = movimiento_parqueadero_id_plaza;
    }

    public Parqueadero getParqueadero_id() {
        return parqueadero_id;
    }

    public void setParqueadero_id(Parqueadero parqueadero_id) {
        this.parqueadero_id = parqueadero_id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Integer getTiempo_parqueadero() {
        return tiempo_parqueo;
    }

    public void setTiempo_parqueadero(Integer tiempo_parqueadero) {
        this.tiempo_parqueo = tiempo_parqueadero;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Boolean getAnulado() {
        return anulado;
    }

    public void setAnulado(Boolean anulado) {
        this.anulado = anulado;
    }

   
    
    
}

