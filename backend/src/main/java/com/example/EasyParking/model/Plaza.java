package com.example.EasyParking.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//import org.hibernate.annotations.GenericGenerator;
/**
 *
 * @author Lindsay Andrea Quintero Hernández
 */
@Entity
@Table(name="plaza")
public class Plaza implements Serializable{
    
    @Id  
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_plaza")
    private Integer id_plaza;
        
    @ManyToOne
    @JoinColumn(name="tipo_vehiculo_id_tipo")
    private TipoVehiculo tipo_vehiculo_id_tipo;
    
    @Column(name="estado")
    private Boolean estado;
    
    @Column(name="nombre")
    private String nombre;

     

    public Integer getId_plaza() {
        return id_plaza;
    }

    public void setId_plaza(Integer id_plaza) {
        this.id_plaza = id_plaza;
    }

    public TipoVehiculo getTipo_vehiculo_id_tipo() {
        return tipo_vehiculo_id_tipo;
    }

    public void setTipo_vehiculo_id_tipo(TipoVehiculo tipo_vehiculo_id_tipo) {
        this.tipo_vehiculo_id_tipo = tipo_vehiculo_id_tipo;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
 
    
}
