
package com.example.EasyParking.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author 
 * @author Lindsay Andrea Quintero Hernández
 */
@Entity
@Table(name="parqueadero")
public class Parqueadero implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    
    @Column(name="nit")
    private Integer nit;
    
    @Column(name="nombre")
    private String nombre;
    
    @Column(name="regimen")
    private String regimen;

//    public Parqueadero(Integer id, Integer nit, String nombre, String regimen) {
//        this.id = id;
//        this.nit = nit;
//        this.nombre = nombre;
//        this.regimen = regimen;
//    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNit() {
        return nit;
    }

    public void setNit(Integer nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

}
