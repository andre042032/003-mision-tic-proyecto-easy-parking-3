/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.servicess;
import com.example.EasyParking.model.TipoVehiculo;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface TipoVehiculoService {
    
    public TipoVehiculo save(TipoVehiculo tipoVehiculo);

    public void delete(Integer id);

    public TipoVehiculo findById(Integer id);

    public List<TipoVehiculo> findAll();

}
