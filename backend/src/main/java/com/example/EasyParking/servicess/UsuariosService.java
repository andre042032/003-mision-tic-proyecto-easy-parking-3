/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.servicess;

import com.example.EasyParking.model.Usuarios;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface UsuariosService {
    public Usuarios save(Usuarios usuario);

    public void delete(Integer id);

    public Usuarios findById(Integer id);

    public List<Usuarios> findAll();
}
