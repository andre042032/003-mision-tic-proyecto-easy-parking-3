/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.servicess;

import com.example.EasyParking.model.Parqueadero;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface ParqueaderoService {
    
    public Parqueadero save(Parqueadero parqueadero);

    public void delete(Integer id);

    public Parqueadero findById(Integer id);

    public List<Parqueadero> findAll();

}
