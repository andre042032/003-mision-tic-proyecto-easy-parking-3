
package com.example.EasyParking.servicess;

import com.example.EasyParking.model.Vehiculo;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface VehiculoService{
    
    public Vehiculo save(Vehiculo vehiculo);
    
    public void delete(Integer id);
    
    public Vehiculo findById(Integer id);
    
    public List<Vehiculo> findAll();
    
}
