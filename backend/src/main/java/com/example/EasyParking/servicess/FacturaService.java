/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.servicess;

import com.example.EasyParking.model.Factura;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface FacturaService {
    public Factura save(Factura factura);

    public void delete(Integer id);

    public Factura findById(Integer id);

    public List<Factura> findAll();
}
