/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.servicess;

import com.example.EasyParking.model.MovimientoParqueadero;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface MovimientoParqueaderoService {
    public MovimientoParqueadero save(MovimientoParqueadero movimientoParqueadero);

    public void delete(Integer id);

    public MovimientoParqueadero findById(Integer id);

    public List<MovimientoParqueadero> findAll();
}
