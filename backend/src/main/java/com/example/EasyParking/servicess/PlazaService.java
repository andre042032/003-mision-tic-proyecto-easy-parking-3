/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EasyParking.servicess;

import com.example.EasyParking.model.Plaza;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface PlazaService {
    public Plaza save(Plaza plaza);

    public void delete(Integer id);

    public Plaza findById(Integer id);

    public List<Plaza> findAll();
}
