import Header from './components/Header'
import Footer from './components/Footer'
import HomeScreen from './screens/HomeScreen'

import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import SelectionScreen from './screens/SelectionScreen'
import IngresoVehiculoScreen from './screens/IngresoVehiculoScreen'
function App() {
  return (
    <BrowserRouter>
      <Header />
      <main className="py-3">
        <Container>
          <Routes>
            <Route path="/" exact element={<HomeScreen />} />
            <Route path="/seleccione-vehiculo" element={<SelectionScreen />} />
            <Route
              path="/ingreso-vehiculo"
              element={<IngresoVehiculoScreen />}
            />
            <Route path="/usuarios" element={<HomeScreen />} />
          </Routes>
        </Container>
      </main>
      <Footer />
    </BrowserRouter>
  )
}
export default App
