import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'

const Footer = () => {
  return (
    <footer>
      <Container>
        <Row>
          <Col className="text-center pt-3">
            Hecho por Equipo Easyparking &copy; {new Date().getFullYear()}
          </Col>
        </Row>
      </Container>
    </footer>
  )
}

export default Footer
