import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap'
import Form from 'react-bootstrap/Form'
const IngresoIdentificacion = ({ tipoVehiculo }) => {
  const [loading, setLoading] = useState(true)
  const [listado, setListado] = useState([])

  useEffect(() => {
    fetch('/plaza/list/')
      .then((response) => response.json())
      .then((data) => {
        setListado(data)
        console.log(listado)
        setLoading(false)
      })
  }, [])

  return (
    <>
      {['carro', 'moto', 'bici'].includes(tipoVehiculo) ? (
        <Form>
          {tipoVehiculo === 'carro' ? (
            <h2 className="text-primary fw-bolder">Digite placa del carro:</h2>
          ) : tipoVehiculo === 'moto' ? (
            <h2 className="text-primary fw-bolder">Digite placa de la moto:</h2>
          ) : (
            tipoVehiculo === 'bici' && (
              <h2 className="text-primary fw-bolder">
                Digite cedula para la bici:
              </h2>
            )
          )}
          <Form.Control type="text" placeholder="Placa / Identificacion" />
          <h3 className="text-primary fw-bolder mt-3">
            Seleccione lugar de parqueo:
          </h3>
          {loading && 'Cargando disponibles...'}
          <Form.Select aria-label="Default select example">
            {loading
              ? 'Cargando disponibles...'
              : listado.map(
                  (plaza) =>
                    !plaza.estado && (
                      <option value={plaza.id_plaza} key={plaza.id_plaza}>
                        {plaza.nombre}
                      </option>
                    )
                )}
          </Form.Select>
          <Button className="mt-3" variant="primary" type="submit">
            Parquear!
          </Button>
        </Form>
      ) : (
        <h1>Tipo de vehiculo no valido</h1>
      )}
    </>
  )
}

export default IngresoIdentificacion
