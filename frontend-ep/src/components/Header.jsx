import React from 'react'
import { Nav, Navbar, Container } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

const Header = (props) => {
  return (
    <>
      <Navbar bg="primary" variant="dark">
        <Container>
          <LinkContainer to="/">
            <Navbar.Brand className="custom-navbar">EasyParking</Navbar.Brand>
          </LinkContainer>
          <LinkContainer to="/">
            <Nav className="mr-auto">
              <Nav.Link>Home</Nav.Link>
            </Nav>
          </LinkContainer>
        </Container>
      </Navbar>
    </>
  )
}

export default Header
