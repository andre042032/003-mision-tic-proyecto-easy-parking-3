import React from 'react'
import background from '../images/logo1.png'
import { Row, Container, Col } from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import { Link } from 'react-router-dom'
const HomeScreen = () => {
  return (
    <div
      style={{
        backgroundImage: `url(${background})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'contain',
        backgroundPosition: 'center 0',
        height: '80vh',
      }}
    >
      <Container className="h-100">
        <Row className="h-100 d-flex">
          <Col className="d-flex ">
            <Link className="d-grid mt-5 mb-auto" to={'/seleccione-vehiculo'}>
              <Button variant="primary" size="lg">
                Soy usuario
              </Button>
            </Link>
          </Col>
          <Col className="d-flex justify-content-end">
            <Link className="d-grid mb-5 mt-auto" to={'/seleccione-vehiculo'}>
              <Button variant="primary" size="lg">
                Soy Admin
              </Button>
            </Link>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default HomeScreen
