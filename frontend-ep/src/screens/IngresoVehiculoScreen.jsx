import React from 'react'

import background from '../images/logo2.png'
import imgCarro from '../images/carro.png'
import imgMoto from '../images/moto.png'
import imgBici from '../images/bici.png'
import { Container, Col, Row } from 'react-bootstrap'
import Image from 'react-bootstrap/Image'
import IngresoIdentificacion from '../components/IngresoIdentificacion'
import { useLocation } from 'react-router-dom'
const IngresoVehiculoScreen = () => {
  const useQuery = () => new URLSearchParams(useLocation().search)
  let query = useQuery()
  let tipoVehiculo = query.get('tipo')

  return (
    <div
      style={{
        backgroundImage: `url(${background})`,
        opacity: '0.5',
        height: '80vh',
      }}
    >
      <Container className="h-100">
        <Row className="h-100">
          <Col>
            <Row className="h-100 align-content-center">
              <IngresoIdentificacion tipoVehiculo={tipoVehiculo} />
            </Row>
          </Col>
          <Col>
            <Row className="h-100 align-content-center">
              {tipoVehiculo === 'carro' ? (
                <Image src={imgCarro}></Image>
              ) : tipoVehiculo === 'moto' ? (
                <Image src={imgMoto}></Image>
              ) : (
                tipoVehiculo === 'bici' && <Image src={imgBici}></Image>
              )}
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default IngresoVehiculoScreen
