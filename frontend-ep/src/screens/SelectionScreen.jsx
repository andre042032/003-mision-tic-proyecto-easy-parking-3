import React from 'react'
import background from '../images/logo2.png'
import Stack from 'react-bootstrap/Stack'
import { Container, Row } from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import { Link } from 'react-router-dom'
const SelectionScreen = () => {
  return (
    <div
      style={{
        backgroundImage: `url(${background})`,
        opacity: '0.5',
        height: '80vh',
      }}
    >
      <Container className="h-100 d-flex align-content-center flex-wrap justify-content-center">
        <Row className="justify-content-center">
          <Stack
            gap={4}
            className="col-md-5 pt-5 mx-auto d-grid gap-2 align-items-center"
          >
            <Link className="d-grid" to={'/ingreso-vehiculo?tipo=carro'}>
              <Button variant="primary" size="lg" active>
                Carro
              </Button>
            </Link>

            <Link className="d-grid" to={'/ingreso-vehiculo?tipo=moto'}>
              <Button variant="primary" size="lg">
                Moto
              </Button>
            </Link>

            <Link className="d-grid" to={'/ingreso-vehiculo?tipo=bici'}>
              <Button variant="primary" size="lg">
                Bici
              </Button>
            </Link>

            <Link className="d-grid" to={'/ingreso-carro'}>
              <Button variant="info" size="lg">
                Retiro mi vehiculo
              </Button>
            </Link>
          </Stack>
        </Row>
      </Container>
    </div>
  )
}

export default SelectionScreen
